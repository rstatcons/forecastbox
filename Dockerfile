FROM registry.gitlab.com/rstatcons/forecastbox/fboxbase

MAINTAINER Alexander Matrunich "a@rresults.consulting"

RUN R -q -e "devtools::install_git(c( \
    'https://gitlab.com/rresults/backendlessr.git', \
    'https://gitlab.com/rresults/paymiller.git', \
    'https://gitlab.com/rstatcons/forecastbox.git'), \
    dependencies = TRUE, \
    repos = 'https://cloud.r-project.org')" \
    && rm -rf /tmp/downloaded_packages/ /tmp/*.rds \
    && mkdir /srv/shiny-server/forecastbox \
    && cd /srv/shiny-server/forecastbox \
    && cp -r /usr/local/lib/R/site-library/forecastbox/apps/proto2/* . \
    && mkdir -p /var/log/shiny-server/fbox \
    && echo "run_as shiny; \
		     server { \
  		       listen 3838; \
  		       location / { \
    		         app_dir /srv/shiny-server/forecastbox; \
    		         directory_index off; \
    		         log_dir /var/log/shiny-server; \
  		       } \
		     }" > /etc/shiny-server/shiny-server.conf \
    && rm /srv/shiny-server/index.html \
    && rm -rf /srv/shiny-server/sample-apps \
    && rm -rf /srv/shiny-server/[0-1][0-9]_*

EXPOSE 3838

CMD ["/usr/bin/shiny-server.sh"]

