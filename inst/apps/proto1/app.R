library(forecastbox)
library(forecast)
library(dygraphs)
library(ggplot2)
library(lubridate)
library(markdown)
library(V8)
library(shiny)
library(shinyjs)
library(purrr)
library(dplyr)

mdls <- c("ets", "bats", "auto.arima")

# To be replaced by a user management backend
psswds <- list(test = "123")

# UI ####
ui <- fluidPage(
  tags$head(includeHTML(
    system.file("google-analytics.html",
                package = "forecastbox"))),
  useShinyjs(),

  titlePanel("Sales Forecast Online"),

  uiOutput("main")
)

# Server ####
server <- function(input, output) {

  # Login ####
  user_logged <- reactiveVal(FALSE)

  observeEvent(
    input$.login,
    if (isTRUE(psswds[[input$.username]] == input$.password)) {
      user_logged(TRUE)
    } else {
      show("message")
      output$message = renderText("Invalid user name or password")
      delay(2000, hide("message", anim = TRUE, animType = "fade"))
    }
               )

  output$main <- renderUI(
    if (user_logged()) ui_tabset_panel_main() else
      ui_login()
    )

  ts1 <- callModule(tsDataInput, "tsdata")

  # Raw data diagnostics plots ####
  output$tsplot <- renderPlot(autoplot(ts1()))
  output$acfplot <- renderPlot(
    ggAcf((ts1()))
  )

  output$seasonplot <- renderPlot({

    validate(
      need(length(ts1()) / frequency(ts1()) > 1.5,
           "Seasonal plot skipped: time series is too short.")
    )

    ggseasonplot(ts1(),
                 year.labels = TRUE,
                 year.labels.left = TRUE)
  }
  )

  output$seasonalplotpolar <- renderPlot({

     validate(
      need(length(ts1()) / frequency(ts1()) > 1.5,
           "Polar seasonal plot skipped: time series is too short.")
    )

     ggseasonplot(ts1(),
                 polar = TRUE)
  }
  )

  output$subseriesplot <- renderPlot({
     validate(
      need(length(ts1()) / frequency(ts1()) >= 2,
           "Subseries plot skipped: time series is too short.")
    )
    ggsubseriesplot(ts1())
  }
  )

  output$lagplot <- renderPlot({
    validate(
      need(length(ts1()) / frequency(ts1()) >= 2,
           "Lag plot skipped: time series is too short.")
    )
      gglagplot(ts1())

  })
  # Build forecast ####

  frcst <- callModule(moduleBuildForecast,
                      "mainforecast",
                      ts1,
                      mdls)

}

# Run the application
shinyApp(ui = ui, server = server)

