suppressPackageStartupMessages({
  library("shiny")
  library("shinyjs")
  library("backendlessr")
  library("paymiller")
  library("forecastbox")
  library("futile.logger")
  library("purrr")
  library("dplyr")
})

forecast_models <- c("ets", "auto.arima", "bats")
# forecast_models <- c("ets", "auto.arima")
howmanytests <- 3L
servicename <- "RResults Forecasting Service"
demomode <- TRUE


logdir <- file.path(
  "/var",
  "log",
  "shiny-server",
  "fbox")

logfile <- paste0(
  format(Sys.time(), "%Y%m%dT%H%M%S"),
  Sys.getpid(),
  ".log")

if (file.exists(logdir)) {
  if (assertthat::is.writeable(logdir)) {
    flog.appender(appender.file(file.path(logdir, logfile)))
  }
}


flog.threshold("TRACE")

plans <- tibble::tribble(
  ~name, ~forecasts, ~price,
  "small", 5L, 5L,
  "standard", 20L, 10L,
  "maxi", 100L, 25L
)

