## 1.	Introduction

1.1	Our website uses cookies.
1.2	By using our website and agreeing to this policy, you consent to our use of cookies in accordance with the terms of this policy.

## 2.	Credit

2.1	This document was created using a template from [SEQ Legal](https://seqlegal.com).

## 3.	About cookies

3.1	A cookie is a file containing an identifier (a string of letters and numbers) that is sent by a web server to a web browser and is stored by the browser. The identifier is then sent back to the server each time the browser requests a page from the server.
3.2	Cookies may be either "persistent" cookies or "session" cookies: a persistent cookie will be stored by a web browser and will remain valid until its set expiry date, unless deleted by the user before the expiry date; a session cookie, on the other hand, will expire at the end of the user session, when the web browser is closed.
3.3	Cookies do not typically contain any information that personally identifies a user, but personal information that we store about you may be linked to the information stored in and obtained from cookies.
3.4	Cookies can be used by web servers to identify and track users as they navigate different pages on a website and identify users returning to a website.


## 4.	Our cookies

4.1	Currently we don't use our own cookies on our website.

## 5.	Analytics cookies

5.1	We use Google Analytics to analyse the use of our website.
5.2	Our analytics service provider generates statistical and other information about website use by means of cookies.
5.3	The analytics cookies used by our website have the following names: _ga, _gat, __utma, __utmt, __utmb, __utmc, __utmz and __utmv.
5.4	The information generated relating to our website is used to create reports about the use of our website.
5.5	Our analytics service provider's privacy policy is available at: [http://www.google.com/policies/privacy/](http://www.google.com/policies/privacy/).

## 6.	Third party cookies

6.1	Our website doesn't third party cookies except analytical cookies (see 5).

## 7.	Blocking cookies

7.1	Most browsers allow you to refuse to accept cookies; for example: 
  (a)	in Firefox (version 51) you can block all cookies by clicking "Tools", "Options", "Privacy", selecting "Use custom settings for history" from the drop-down menu, and unticking "Accept cookies from sites"; and
  (b)	in Chrome (version 55), you can block all cookies by accessing the "Customise and control" menu, and clicking "Settings", "Show advanced settings" and "Content settings", and then selecting "Block sites from setting any data" under the "Cookies" heading.
7.2	Blocking all cookies will have a negative impact upon the usability of many websites.
7.3	If you block cookies, you will not be able to use all the features on our website.

## 8.	Deleting cookies

8.1	You can delete cookies already stored on your computer; for example:
  (a)	in Firefox (version 51), you can delete cookies by clicking "Tools", "Options" and "Privacy", then selecting "Use custom settings for history" from the drop-down menu, clicking "Show Cookies", and then clicking "Remove All Cookies"; and
  (b)	in Chrome (version 55), you can delete all cookies by accessing the "Customise and control" menu, and clicking "Settings", "Show advanced settings" and "Clear browsing data", and then selecting "Cookies and other site and plug-in data" before clicking "Clear browsing data".
8.2	Deleting cookies will have a negative impact on the usability of many websites.

## 9.	Our details

9.1	Our details are provided on About us page.

