This webservice is owned and operated by [RResults Consulting OÜ](rresults.consulting).

We are registered in Estonia under registration number 12987939, and our registered office is at Paju 20-36, Vöru, 65610 Estonia.

Our principal place of business is at Hospital Exit 3-68, Tbilisi, 0113 Georgia.

You can contact us:

* by email [info@salesforecast.online](mailto:info@salesforecast.online);
* by telephone +995 555576954 on weekdays 9am-6pm UTC+4;
* by post, to the postal addresses given above.
