context("Manage PAYMILL data")
library("backendlessr")
library("paymiller")
user <- Sys.getenv("TESTUSER2")
pass <- Sys.getenv("TESTUSER2_PASS")

test_that("PAYMILL ID is retrieved", {
  skip_on_cran()
  skip_if_not(!identical(pass, ""))
  usr_sssn <- bel_session(bel_users_login(user, pass))
  skip_if_not(is_paymill_client(usr_sssn))
  expect_match(paymill_client_id(usr_sssn),
               "^client_[a-z0-9]{20}$")
  usr_sssn <- bel_users_logout(usr_sssn)
})

test_that("Corresponding user in PAYMILL is removed", {
  skip_on_cran()
  skip_if_not(!identical(pass, ""))
  usr_sssn <- bel_session(bel_users_login(user, pass))
  skip_if_not(is_paymill_client(usr_sssn))
  usr_sssn <- remove_paymill_client(usr_sssn)
  expect_false(is_paymill_client(usr_sssn))
  usr_sssn <- bel_users_logout(usr_sssn)
})

test_that("Corresponding user in PAYMILL is created", {
  skip_on_cran()
  skip_if_not(!identical(pass, ""))
  usr_sssn <- bel_session(bel_users_login(user, pass))
  skip_if_not(!is_paymill_client(usr_sssn))
  usr_sssn <- register_paymill_client(usr_sssn)
  expect_true(is_paymill_client(usr_sssn))
  usr_sssn <- bel_users_logout(usr_sssn)
})
