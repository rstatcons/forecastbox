# Forecastbox: web-application for time-series forecasting

The aim of the Forecastbox web-application is to empower business decision makers with state-of-art forecasting methods. A user doesn't need to have statistical knowledge for prediction of their time series. The Forecastbox automatically selects best forecasting method for user's time series and calculates predictions based on this method.  

The Forecastbox is built with R programming language and uses RStudio [Shiny framework](https://shiny.rstudio.com/) for user interface. The workhorse of the app is [`forecast` R package](https://cran.r-project.org/package=forecast). The Forecastbox includes components to provide user logins through [`backendlessr` R package](https://gitlab.com/rresults/backendlessr)  and to accept online payments through [`paymillr` R package](https://gitlab.com/rresults/paymiller/).

